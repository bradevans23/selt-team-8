require 'spec_helper'
require 'rails_helper'

describe ShirtsController do
 describe 'showing the products of a category' do
   it 'should redirect to the category page if no category is selected' do
     post :show
     expect(response).to redirect_to '/shirts'
   end
 end
 describe 'showing an individual product' do
   it 'should redirect to the category page if no product is selected' do
     post :show_product
     expect(response).to redirect_to '/shirts'
   end
 end
end
