class Group < ActiveRecord::Base
    
    has_many :memberships
    has_many :users, through: :memberships
    
    def self.create_group!(group_params)
        group_params[:group_id] = loop do
          token = SecureRandom.urlsafe_base64
          break token unless Group.exists?(:group_id => token)
        end
        #group_params[:group_id]=SecureRandom.base64
        Group.create!(group_params)
    end
    
    def self.add_members!(accept_params)
        #puts group_params
        #Membership.create!(accept_params)
    end
end
