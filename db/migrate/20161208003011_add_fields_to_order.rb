class AddFieldsToOrder < ActiveRecord::Migration
  def change
    drop_table :orders
    create_table :orders do |t|
      t.string :order_number
      t.string :user_id
      t.string :group_id
      t.string :design_id
      t.datetime :ordered_at
    end
  end
end
