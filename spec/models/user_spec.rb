require 'rails_helper'

    # t.string   "email",                  default: "", null: false
    # t.string   "encrypted_password",     default: "", null: false
    # t.string   "reset_password_token"
    # t.datetime "reset_password_sent_at"
    # t.datetime "remember_created_at"
    # t.integer  "sign_in_count",          default: 0,  null: false
    # t.datetime "current_sign_in_at"
    # t.datetime "last_sign_in_at"
    # t.string   "current_sign_in_ip"
    # t.string   "last_sign_in_ip"
    # t.datetime "created_at",                          null: false
    # t.datetime "updated_at",                          null: false
    # t.string   "firstname"
    # t.string   "lastname"
    # t.string   "username"
    # t.string   "size"

RSpec.describe User, type: :model do
  
 
      describe 'a valid entry' do
          
        subject { described_class.new(email: "testing", encrypted_password: "asdfasdf", sign_in_count: 0, size: "s", created_at: DateTime.now, updated_at: DateTime.now + 1.week) }
          
        it "is valid with valid attributes" do
            expect(subject).to be_valid
        end
        
        it "is not valid without an emial" do
            subject.email = nil
            expect(subject).to_not be_valid
        end
        it "is not valid without a password" do
            subject.encrypted_password = nil
            expect(subject).to_not be_valid
        end
        it "is valid with a firstname" do
            expect(subject.firstname).to be_valid
        end
        
        it "is valid with valid size" do
            expect(subject.size).to be_valid
        end
      end
  
end