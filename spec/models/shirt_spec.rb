require 'rails_helper'

    # t.string   "design_id"
    # t.string   "group_id"
    # t.string   "end_date"
    # t.string   "delivery_address"
    # t.datetime "created_at"
    # t.datetime "updated_at"

RSpec.describe Shirt, type: :model do
  
      describe 'a valid entry' do

      subject {
            described_class.new(design_id: "Anything", group_id: "Lorem ipsum", end_date: "testing",
                      created_at: DateTime.now, updated_at: DateTime.now + 1.week, delivery_address: "test address")
        }
        
        it "is valid with valid attributes" do
            expect(subject).to be_valid
        end
        
        it "is not valid without a group_id" do
            subject.group_id = nil
            expect(subject).to_not be_valid
        end
        it "is not valid without a delivery_address" do
            subject.delivery_address = nil
            expect(group).to_not be_valid
        end
        it "is not valid without a design_id" do
            subject.design_id = nil
            expect().to_not be_valid
        end
        
    end
  
  
end