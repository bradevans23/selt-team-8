require 'spec_helper'
require 'rails_helper'

describe HomeController do
  
  include Devise::TestHelpers
  before(:each) do
      login_as create(:user) , scope: :user
  end
  describe '#index' do
    it 'should show groups' do
      @user = User.create!(:email => 'email@email.com', :password => 'password', :password_confirmation => 'password')
      login_with @user
      get :index
      expect(response).to render_template("home/index")
    end
    
  end
end
      