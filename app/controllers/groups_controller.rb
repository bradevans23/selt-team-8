class GroupsController < ApplicationController

  def group_params
    params.require(:group).permit(:group_id, :admin_id, :group_name, :accept => [])
  end
  
  def acceptDeny_params
    params.require(:acceptDeny).permit(:accept, :deny)
  end

  def show
    @group = Group.find_by(:group_id => params[:group_id])
    @groups = Group.where(:admin_id => current_user.id) #need this here for navbar to work on groups pages
    @memberships = Membership.where(:group_id => params[:group_id])
    @members = Membership.where(:group_id => params[:group_id], :accepted => true)
    puts @members
    @newusers = {}
    cookies[:group_id]=params[:group_id]
    @memberships.each do |membership|
      if membership.accepted == false
        
        @addUser = User.find_by(:email => membership.user_id)
        if !@addUser.nil?
          @newusers[@addUser] = membership
          puts @newusers
        else
          puts "not found"
          puts membership.user_id
        end
      end
    end
  end

  def index
    
  end

  def new
    # default: render 'new' template
    @groups = Group.where(:admin_id => current_user.id) #need this here for navbar to work on groups pages
  end

  def create
    
    if !current_user.nil?
      group_params[:admin_id]=current_user.id
      Group.create_group!(group_params)
      
    end
    redirect_to authenticated_root_path
  end

  def edit
    
  end

  def update
    @newMembersToDeny = params[:deny]
    @newMembersToAccept = params[:accept]
    if @newMembersToDeny.nil?
      @newMembersToDeny = []
    end
    if @newMembersToAccept.nil?
      @newMembersToAccept = []
    end
    
    @checkedBoth = []

    if(!(@newMembersToDeny.empty? || @newMembersToAccept.empty?))
      @newMembersToDeny.each do |user|
          puts user
          if @newMembersToAccept.include?(user)
            puts "checked both"
            @checkedBoth << user
          end
      end
    end
    
    if(!@newMembersToDeny.nil?)
      @newMembersToDeny.each do |user, membership|
        if(!@checkedBoth.include?(user))
          @toDelete = Membership.find_by(:user_id => user, :group_id => params[:group][:group_id] )
          @toDelete.destroy
        end
      end
    end
   
    if(!@newMembersToAccept.nil?)
      @newMembersToAccept.each do |user|
        if(!@checkedBoth.include?(user))
          puts user
          @toChange = Membership.find_by(:user_id => user, :group_id => params[:group][:group_id] )
          @toChange.update_attribute(:accepted, true)
          puts @toChange
        end
      end
    end
    
    redirect_to group_path(:group_id => params[:group][:group_id])
  end

  def destroy
    
  end

end
