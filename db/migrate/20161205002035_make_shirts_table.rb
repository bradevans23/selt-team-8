class MakeShirtsTable < ActiveRecord::Migration
  def change
    drop_table :shirts
    create_table :shirts do |t|
      t.string :design_id
      t.string :group_id
      t.string :end_date
      t.string :delivery_address
      t.timestamps
    end
  end
end
