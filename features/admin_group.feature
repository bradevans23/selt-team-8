Feature: Admin my group
  
Background: Start on Home page
  
  Given I am logged in
  And I am on the home page
  And I press "createGroup"
  And I fill in "signup_user" with "testGroup"
  And I press "signup_create"
  And I log out
  And A User logs in
  
Scenario: Accept User into Group