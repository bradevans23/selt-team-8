Given /^I am on the login page$/ do
  visit root_path
end


Given /^I am on the signup page$/ do
   visit new_user_registration_path
end

When /^I fill in "(.*?)" with "(.*?)"$/ do |field, val|
    fill_in field, :with => val
end

And /^I press "(.*?)"$/ do |button|
    click_on button
end

Then /^I should see "(.*?)"$/ do |content|
    expect(page).to have_content(content)
end

And /^I select "(.*?)" from "(.*?)"$/ do |size, sizes|
    page.find_by_id('size').find("option[value='20120905']").select_option
end

When 'I fill in Login Information' do
  email = 'testing@man.net'
  password = 'secretpass'
  User.new(:email => email, :password => password, :password_confirmation => password).save!   
  fill_in "email", :with => email
  fill_in "password", :with => password
end

When /^I am on "(.*?)"$/ do |path|
    visit path
end

And "I log out" do
   click_on 'Logout' 
end