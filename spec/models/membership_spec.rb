require 'rails_helper'

    # t.string  "user_id"
    # t.string  "group_id"
    # t.boolean "accepted"
    
RSpec.describe Membership, type: :model do
  
    describe 'a valid entry' do

      subject {
            described_class.new(user_id: "Anything", group_id: "Lorem ipsum", accepted: "testing")
        }
        
        it "is valid with valid attributes" do
            expect(subject).to be_valid
        end
        
        it "is not valid without a group_id" do
            subject.group_id = nil
            expect(subject).to_not be_valid
        end
        it "is not valid without a user_id" do
            subject.user_id = nil
            expect(group).to_not be_valid
        end
        it "is not valid without a accepted" do
            subject.accepted = nil
            expect().to_not be_valid
        end
        
    end
  
end
