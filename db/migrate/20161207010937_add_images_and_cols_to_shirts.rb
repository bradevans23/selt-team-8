class AddImagesAndColsToShirts < ActiveRecord::Migration
  def change
    add_attachment :shirts, :image
    add_column :shirts, :color, :string
  end
end
