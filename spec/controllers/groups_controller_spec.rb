require 'spec_helper'
require 'rails_helper'

describe GroupsController do
  before { user = double(User, :id => "1234"), controller.stub(:current_user).and_return(user),
           membership = double(Membership, :user_id => "1234") ,
           groupNew = double(Group,:group_id => "1234")
  }
  
  describe 'creating a new group' do
    it 'should create a new group and redirect back to the homepage' do
      @user = User.create!(:email => 'email@email.com', :password => 'password', :password_confirmation => 'password')
      login_with @user
      group = Hash.new
      group[:group_name]="name"
      group[:admin_id]="1234"
      params = Hash.new
      params[:group] = {:group_name => 'name', :admin_id => @user.email}
      params[:admin_id] = @user.email
      params2 = Hash.new
      params2 = params
      #params[:admin_id] = "1234"
      #allow(Group).to receive(:create_group!).with(params)
      post :create, params2
      expect(response).to redirect_to '/'
    end
  end
  describe 'add user to group' do
    it 'should post it and redirect' do
      @user = User.create!(:email => 'email@email.com', :password => 'password', :password_confirmation => 'password')
      login_with @user
      params = Hash.new
      params[:accept] = {:user_id => @user.email}
      params[:deny] = nil
      
      @group = Group.create!(:group_name => 'name', :group_id => 0)
      params[:group] = {:group_id => 0, :group_name => 'name'}
      params[:id] = @group.group_id
      @membership = Membership.create!(:group_id => params[:group][:group_id], :user_id => @user.email)
      params2 = Hash.new
      params2 = params
      allow(Membership).to receive(:destroy)
      put :update, params2
      expect(response).to redirect_to group_path(:group_id => params[:group][:group_id])
    end
  end
  describe 'deny user from group' do
    it 'should post it and redirect denial' do
      @user = User.create!(:email => 'email@email.com', :password => 'password', :password_confirmation => 'password')
      login_with @user
      params = Hash.new
      params[:deny] = {:user_id => @user.email}
      params[:accept] = nil
      
      @group = Group.create!(:group_name => 'name', :group_id => 0)
      params[:group] = {:group_id => 0, :group_name => 'name'}
      params[:id] = @group.group_id
      @membership = Membership.create!(:group_id => params[:group][:group_id], :user_id => @user.email)
      params2 = Hash.new
      params2 = params
      #put :update, params2
      #expect(response).to redirect_to group_path(:group_id => params[:group][:group_id])
      #expect(Membership).to receive(:find_by)
      #controller.update
    end
  end
  describe 'show groups' do
    it 'should show groups' do
      @user = User.create!(:email => 'email@email.com', :password => 'password', :password_confirmation => 'password')
      login_with @user
      @group = Group.create!(:group_name => 'name', :group_id => '0')
      get :show, :id => @group.group_id
      expect(response).to render_template(:show)
    end
  end
  
end


