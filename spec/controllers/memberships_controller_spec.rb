require 'rails_helper'

RSpec.describe MembershipsController, type: :controller do
    include Devise::TestHelpers
    before(:each) do
        login_as create(:user) , scope: :user
    end
    
    describe "#create" do
    it "creates a successful membership" do
      @user = User.create!(:email => 'email@email.com', :password => 'password', :password_confirmation => 'password')
      login_with @user
      #expect(@user.email).to eql('eql')
      post :create, {:group_id => 'group_id'}
      #@membership = Membership.create(:group_id => 'group_id', :user_id => @user.email)
      #expect(@membership.group_id).to eql("group_id")
      #expect(@membership.user_id).to eql('email@email.com')
      expect(response).to redirect_to('/')
      #expect(Membership).to receive(:create)
    end
  end
end
