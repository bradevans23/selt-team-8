Feature: Create New Group
  
Background: Start on Home page
  
  Given I am logged in
  And I am on the home page

Scenario: Go to Create New Group
  
  When I press "createGroup"
  Then I should see "Create Group"

Scenario: Create New Group
  
  When I press "createGroup"
  And I fill in "signup_user" with "testGroup"
  And I press "signup_create"
  Then I should see "Your group: testGroup has been created with the administrator:"
  
Scenario: Cancel group creation
  
  When I press "createGroup"
  And I press "cancel"
  Then I should see "Homepage for "