require 'rails_helper'

#describe means example groups
RSpec.describe CartController, type: :controller do
  
  #group inside of a group
  describe "GET #index" do
    # it = example
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
      #syntax for expect
      #expect(  ).to(   )
      #expect(  ).not_to(   )
    end
  end
  
  describe "GET #add" do
    it "adds item to cart" do
      #expectations
    end
  
  end
  
  describe "Get #clearCart" do
    it "clears user cart" do
      #expectations
    end
    
  end

end
