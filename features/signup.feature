Feature: signup user
  
Background: Start on signup page
  
  Given I am on the signup page
  
Scenario: Sign up new user valid
  
  And I fill in "firstname" with "firstname"
  And I fill in "lastname" with "lastname"
  And I fill in "email" with "newuser@email.com"
  And I fill in "username" with "username"
  And I fill in "password" with "password"
  And I fill in "password_confirmation" with "password"
  #And I select "S" from "sizes"
  And I press "Sign up"
  Then I should see "Welcome! You have signed up successfully."
  
Scenario: Sign up new user without email
    And I fill in "firstname" with "firstname"
    And I fill in "lastname" with "lastname"
    And I fill in "username" with "username"
    And I fill in "password" with "password"
    And I fill in "password_confirmation" with "password"
   # And I select "S" from "sizes"
    And I press "Sign up"
    Then I should see "Email can't be blank"
    
Scenario: Sign up new user without password
    And I fill in "firstname" with "firstname"
    And I fill in "lastname" with "lastname"
    And I fill in "email" with "newuser@email.com"
    And I fill in "username" with "username"
  #  And I select "S" from "sizes"
    And I press "Sign up"
    Then I should see "Password can't be blank"