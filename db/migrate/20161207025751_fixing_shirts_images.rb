class FixingShirtsImages < ActiveRecord::Migration
  def change
    remove_column :shirts, :image_file_name
    remove_column :shirts, :image_content_type
    remove_column :shirts, :image_file_size
    remove_column :shirts, :image_updated_at
    add_column :shirts, :image_url, :string
  end
end
