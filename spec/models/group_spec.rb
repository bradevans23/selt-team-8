require 'spec_helper'
    # items in group 
    # t.string   "group_id"
    # t.string   "admin_id"
    # t.string   "group_name"
    # t.datetime "created_at"
    # t.datetime "updated_at"
    # t.integer  "group_id_id"


describe Group do
    describe 'creating group' do
        it 'should assign attribute values correctly for new group in database' do
            group_params = Hash.new
            group_params[:admin_id]="1234"
            group_params[:group_name]="name"
            test_result = Group.create_group!(group_params)
            expect(test_result[:admin_id]).to eq("1234")
            expect(test_result[:group_name]).to eq('name')
        end
    end
    
    describe 'a valid entry' do

      subject {
            described_class.new(group_id: "Anything", admin_id: "Lorem ipsum", group_name: "testing",
                      created_at: DateTime.now, updated_at: DateTime.now + 1.week, group_id_id: 43)
        }
        
        it "is valid with valid attributes" do
            expect(subject).to be_valid
        end
        
        it "is not valid without a group_id" do
            subject.group_id = nil
            expect(subject).to_not be_valid
        end
        it "is not valid without a group_name" do
            subject.group_name = nil
            expect(group).to_not be_valid
        end
        it "is not valid without a admin_id" do
            subject.admin_id = nil
            expect().to_not be_valid
        end
        
        it "is valid with valid attributes" do
            expect(subject).to be_valid
        end
        
    end
    
    

end
