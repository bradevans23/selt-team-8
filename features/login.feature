Feature: login from login page
  
Background: Start on Login page
  
  Given I am on the login page
  

Scenario: Sign up new user
  
  When I press "Sign up"
  And I fill in "firstname" with "firstname"
  And I fill in "lastname" with "lastname"
  And I fill in "email" with "newuser@email.com"
  And I fill in "username" with "username"
  And I fill in "password" with "password"
  And I fill in "password_confirmation" with "password"
  #And I select "S" from "sizes"
  And I press "Sign up"
  Then I should see "Welcome! You have signed up successfully."
  
Scenario: Login as a User
  
  When I fill in Login Information
  And I press "Log in"
  Then I should see "Signed in successfully."
  
Scenario: Invalid Login Info
  
  When I fill in "email" with "badAcct@email.com"
  And I press "Log in"
  Then I should see "Invalid Email or password."
  
