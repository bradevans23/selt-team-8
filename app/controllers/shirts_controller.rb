class ShirtsController < ApplicationController


  def shirt_params
    params.require(:shirt).permit(:group_id, :design_id, :end_date, :delivery_address, :image_url, :color, :product, :price)
  end

  def show
    if params[:category].nil?
      redirect_to shirts_path
    else
      @category=params[:category]
      @products = JSON.parse(RestClient.get("https://test_pNOw-AjW1Pz73UNPKb3F2g:@api.scalablepress.com/v2/categories/#{params[:category]}"))["products"]
      
      
    end
    @groups = Group.where(:admin_id => current_user.id) #need this here for navbar to work on shirts pages
  end

  def show_product
    if params[:product].nil?
      redirect_to shirts_path
    else
      @product = JSON.parse(RestClient.get("https://test_pNOw-AjW1Pz73UNPKb3F2g:@api.scalablepress.com/v2/products/#{params[:product]}"))
      
    end
    @groups = Group.where(:admin_id => current_user.id) #need this here for navbar to work on shirts pages
  end
  
  

  def index
    cat = JSON.parse(RestClient.get("https://test_pNOw-AjW1Pz73UNPKb3F2g:@api.scalablepress.com/v2/categories"))
    @categories = cat.select {|category| category["type"]=="Garment"}
    @groups = Group.where(:admin_id => current_user.id) #need this here for navbar to work on shirts pages
  end

  def new
 
    @shirtProduct = JSON.parse(RestClient.get("https://test_pNOw-AjW1Pz73UNPKb3F2g:@api.scalablepress.com/v2/products/#{params[:shirt][:product]}"))
    params[:shirt][:group_id]=cookies[:group_id]
    
    
    if shirt_params[:price].to_f < 0
      return
    end
    testShirt = Shirt.create_shirt!(shirt_params)
    redirect_to authenticated_root_path
  end

  def create
    
  end

  def edit
    
  end

  def update
    
  end

  def destroy
    
  end
  
  def upload
    
  end

end
