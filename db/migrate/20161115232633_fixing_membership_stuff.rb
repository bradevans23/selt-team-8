class FixingMembershipStuff < ActiveRecord::Migration
  def change
    remove_column :memberships, :group_id
    add_column :memberships, :group_id, :string
  end
end
