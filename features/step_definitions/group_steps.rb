Given 'I am logged in' do
    visit root_path
    email = 'testing@man.net'
    password = 'secretpass'
    User.new(:email => email, :password => password, :password_confirmation => password).save!   
    fill_in "email", :with => email
    fill_in "password", :with => password
    click_on 'Log in'
end

And 'I am on the home page' do
   visit  authenticated_root_path
end