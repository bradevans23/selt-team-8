class MembershipsController < ApplicationController
  
  def memberships_params
    params.require(:membership).permit(:group_id, :user_id)
  end

  def show
    
  end

  def index
    
  end

  def new
    # default: render 'new' template
  end

  def create
    acceptParams = {}
    acceptParams[:user_id] = current_user.email
    puts acceptParams[:user_id]
    acceptParams[:group_id] = params[:group_id]
    acceptParams[:accepted] = false
    puts acceptParams
    Membership.create!(acceptParams)
   # puts params[:group]
    redirect_to authenticated_root_path
  end

  def edit
    
  end

  def update
    
  end

  def destroy
    
  end
end
