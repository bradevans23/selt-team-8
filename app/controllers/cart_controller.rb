class CartController < ApplicationController
    
  # before_action :authenticate_user!, except: [:index] 
  before_action :authenticate_user!
  require "rubygems"
  require "braintree"

  Braintree::Configuration.environment = :sandbox
  Braintree::Configuration.merchant_id = 'c2hnhbnsn3d7b22r'
  Braintree::Configuration.public_key = 'xwfkwq7gs5hkz4t8'
  Braintree::Configuration.private_key = 'ea3d0518927b6353c2789ae39500762f'
 
  TRANSACTION_SUCCESS_STATUSES = [
    Braintree::Transaction::Status::Authorizing,
    Braintree::Transaction::Status::Authorized,
    Braintree::Transaction::Status::Settled,
    Braintree::Transaction::Status::SettlementConfirmed,
    Braintree::Transaction::Status::SettlementPending,
    Braintree::Transaction::Status::Settling,
    Braintree::Transaction::Status::SubmittedForSettlement,
  ]
  
  def add
    id = params[:id]
      #if the cart has already been created, use the existing cart else created, use the exisitng cart else create a new cart
      if session[:cart] then
        cart = session[:cart]
      else
        session[:cart] = {}
        cart = session[:cart]
      end
      if cart[params[:id]]
        puts "already in cart"
      else
        cart[params[:id]] = params[:id]
      end
    redirect_to :action => :index
    
  end # end add method
  
  def clearCart
    session[:cart] = nil
    redirect_to :action => :index
  end
  
  def index
    
    @groups = Group.where(:admin_id => current_user.id)
    @allgroups = Group.all
    @currentUser = nil
    @clientToken = Braintree::ClientToken.generate
    if !current_user.nil?
      @currentUser = current_user
    end
    #if there is a cart, pass it to the page for display else pass an empty value
    if session[:cart] then
      @cart = session[:cart]
      @shirts = []
      @cart.each do |key,designID|
        shirt = Shirt.where(:design_id => designID)
        shirt.each do |s|
            if s.product
              #puts s.product
              newS = Hash.new()
              newS["shirt"] = s
              
              product = JSON.parse(RestClient.get("https://test_pNOw-AjW1Pz73UNPKb3F2g:@api.scalablepress.com/v2/products/#{s.product}"))
              #price = JSON.parse(RestClient.get("https://test_pNOw-AjW1Pz73UNPKb3F2g:@api.scalablepress.com/v2/products/#{s.product}/items"))
              
             
              newS["product"] = product
              @shirts.push(newS)
              #puts s
          end
        end
        
      end
    else
      @cart = {}
    end
  end

  
  
  def checkout
      amount = params["amount"]
      nonce = params["payment_method_nonce"]
      
      result = Braintree::Transaction.sale(
        amount: amount,
        payment_method_nonce: nonce,
        :options => {
          :submit_for_settlement => true
        })
      if result.success? || result.transaction
        
        #clear the cart here
        cart_params = {}
        cart_params[:cart] = session[:cart]
        cart_params[:user_id] = current_user.email
        Order.create_order!(cart_params)
        session[:cart] = {}
        redirect_to authenticated_root_path
      else
        error_messages = result.errors.map { |error| "ERROR: #{error.code}: #{error.message}" }
      
      
        redirect_to authenticated_root_path
      end
      
  end
end

